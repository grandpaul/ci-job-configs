#!/bin/bash

set -ex
echo "source: $source"
echo "zip: $zip"
echo "repo: $repo"
incoming=/var/lib/machines/obs/srv/repos/${repo}/mini-dinstall/incoming/

rm -rf tmp/
mkdir -p tmp
cd tmp
if [ "$zip" == "false" ]
then
    dget -q -d -u $source
    sourcename=`basename $source|sed -e 's,_.*,,'`
    echo "will upload to repo: $repo $sourcename"
    cp * $incoming
else
    wget -O artifact.zip $source
    unzip artifact.zip
    cp debian/output/* $incoming
fi

/usr/bin/mini-dinstall -v --batch --config=/home/buildslave/.mini-dinstall-${repo}.conf

