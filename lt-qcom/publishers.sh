#!/bin/bash

set -ex

if [ ! -d "out" ]; then
    echo "Avoid publishing, not out directory exists."
    exit 0
fi

# Create MD5SUMS file
(cd out && md5sum $(find . -type f) > MD5SUMS.txt)

wget -q ${BUILD_URL}consoleText -O out/build-log-$(echo ${JOB_NAME}|sed -e 's/[^A-Za-z0-9._-]/_/g')-${BUILD_NUMBER}.txt

# Publish to snapshots
test -d ${HOME}/bin || mkdir ${HOME}/bin
wget https://git.linaro.org/ci/publishing-api.git/blob_plain/HEAD:/linaro-cp.py -O ${HOME}/bin/linaro-cp.py

# TODO CLO: Remove condition once linaro-cp.py is able to push to CLO
if [[ -z ${CLO_MIGRATION} ]]; then
    time python3 ${HOME}/bin/linaro-cp.py \
         --server ${PUBLISH_SERVER} \
         --link-latest \
         out ${PUB_DEST}
else
    cd out
    time ${HOME}/bin/jfrog rt u \
         --flat=false --include-dirs=true --symlinks=true --detailed-summary \
         --apikey ${LT_QCOM_CLO_API_KEY} \
         --url ${PUBLISH_SERVER} \
         "*" ${PUB_DEST}/
fi
