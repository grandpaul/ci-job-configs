#!/bin/bash

set -ex

# Get tuxsuite
virtualenv --python=$(which python3) .venv
source .venv/bin/activate
pip install tuxsuite
tuxsuite --version

# When testing a Github PR, use the appropriate ref
if [ "${ghprbPullId}" ]; then
    BRANCH="${ghprbTargetBranch}"
    REF="ref"
    QCOM="refs/pull/${ghprbPullId}/merge"

# otherwise it's a regular CI build
else
    REF="branch"
    QCOM="${BRANCH}"
fi

# Create Tux Job file
cat << EOF > tux.json
{
  "sources": {
    "git_trees": [
      {
        "url": "${POKY_URL}",
        "branch": "${BRANCH}"
      },
      {
        "url": "https://github.com/ndechesne/meta-qcom",
        "${REF}": "${QCOM}"
      }
    ]
  },
  "container": "ubuntu-20.04",
  "envsetup": "poky/oe-init-build-env",
  "distro": "${DISTRO}",
  "machine": "${MACHINE}",
  "target": "${IMAGES}",
  "bblayers_conf": [
    "BBLAYERS += '../meta-qcom/'"
  ],
  "artifacts": [],
    "environment": {
  },
  "local_conf": [
    "INHERIT += 'buildstats buildstats-summary'",
    "TCLIBC := '${TCLIBC}'"
  ]
}
EOF

# Build, do not report tuxsuite return code, since we want to process json out
tuxsuite bake submit --json-out status.json tux.json || true

# cleanup virtualenv
deactivate

if [ ! -f status.json ]; then
    echo "tux suite failed, infrastructure error, status.json does not exist"
    exit 1
fi

url=$(cat status.json | jq -r ".download_url")
state=$(cat status.json | jq -r ".state")
result=$(cat status.json | jq -r ".result")

if [ "$state" != "finished" ]; then
    echo "tuxsuite failed, with an unexpected reason"
    exit 1
fi

wget $url/build.log
cat build.log

echo "TUXBUILD_URL=$url" > parameters

if [ "$result" != "pass" ]; then
    echo "tuxsuite build failed"
    exit 2
fi
