if (manager.build.result == hudson.model.Result.SUCCESS) {
  def url = manager.envVars["TUXBUILD_URL"]

  def desc = "&nbsp;<a href='${url}'>Build location</a><br />"

  manager.build.setDescription(desc)
}
