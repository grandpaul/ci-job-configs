#!/bin/bash

set -ex

sudo apt-get update
sudo apt-get install -y ccache bc kmod cpio chrpath gawk texinfo libsdl1.2-dev whiptail diffstat libssl-dev build-essential libgmp-dev libmpc-dev python3 python3-pip python3-setuptools yamllint libyaml-dev swig rsync python3-dev python3-setuptools-scm python3-wheel

pip3 install --user dtschema

# install jflog client tool, v1, used for publishing artifacts
(mkdir -p $HOME/bin && cd $HOME/bin && curl -fL https://getcli.jfrog.io | sh)
