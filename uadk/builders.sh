#!/bin/bash -e

export UADK_VER_ENV="v2"
LOCK_FILE="/var/lock/uadk-lock"
BIN_DIR="usr/local/bin"
INC_DIR="usr/local/include"
LIB_DIR="usr/local/lib"

lock() {
	exit_code=1
	pending=0
	while [ "$exit_code" != 0 ]; do
		exit_code=0
		mkdir ${LOCK_FILE} &> /dev/null || exit_code=$?
		#mkdir ${LOCK_FILE} || exit_code=$?
		if [ "$exit_code" != 0 ]; then
			if [ "$pending" = 0 ]; then
				# Some script is accessing hardware
				echo "Wait for other building script finishing."
				pending=1
			fi
		fi
	done
}

unlock() {
	if [ -d ${LOCK_FILE} ]; then
		rmdir ${LOCK_FILE}
		echo "Release lock"
	fi
}

ctrlc_handler() {
	echo "Ctrl-C caught...performing clean up"
	unlock
	# Exit with error code
	exit 2
}

# Check whether prebuilt package exists.
# If it exists, install binaries and libraries directly.
check_prebuilt_package() {
	exit_code=0
	cd ${WORKSPACE}
	case $1 in
	"openssl")
		if [ -d ${WORKSPACE}/openssl ]; then
			cd ${WORKSPACE}/openssl
			git checkout OpenSSL_1_1_1a || exit_code=$?
			if [ "$exit_code" != 0 ]; then
				cd ${WORKSPACE}
				rm -fr ${WORKSPACE}/openssl
				git clone https://github.com/openssl/openssl.git
				cd ${WORKSPACE}/openssl
			fi
		else
			git clone https://github.com/openssl/openssl.git
			cd ${WORKSPACE}/openssl
		fi
		git checkout OpenSSL_1_1_1a
		COMMIT=`git log HEAD^..HEAD | grep commit`
		NAME=`ls ../openssl-*.tar.xz`
		NAME=${NAME#*openssl-}
		;;
	*)
		echo "Invalid package name: $1"
		return 1
		;;
	esac
	NAME=`echo ${NAME} | cut -b 1-8`
	if [ -z ${NAME} ]; then
		echo "Can not find package ${NAME}."
		return 1
	fi
	COMMIT=${COMMIT#*commit }
	COMMIT=`echo ${COMMIT} | cut -b 1-8`
	if [ ${NAME} = ${COMMIT} ]; then
		echo "Prebuilt package ${NAME} is found."
		cd ${WORKSPACE}
		case $1 in
		"openssl")
			rm -fr ${WORKSPACE}/openssl
			tar -xJf openssl-${NAME}.tar.xz
			cd ${WORKSPACE}/openssl
			make install &> /dev/null
			;;
		esac
		return 0
	fi
	echo "Package name ${NAME} could not match commit name ${COMMIT}."
	return 1
}

# Check whether repo is downloaded since pull request occurs.
# If it exists, skip to fetch repo with master branch.
check_existed_repo() {
	exit_code=0
	cd ${WORKSPACE}
	case $1 in
	"uadk")
		rm -fr ${WORKSPACE}/uadk-shared-v2
		rm -fr ${WORKSPACE}/uadk-static-v2
		if [ ! -d ${WORKSPACE}/uadk ]; then
			echo "Clone UADK repo."
			git clone --depth 1 https://github.com/Linaro/uadk.git
		fi
		cd ${WORKSPACE}/uadk
		;;
	"ssluadk")
		if [ ! -d ${WORKSPACE}/uadk_engine ]; then
			echo "Clone OPENSSL-UADK repo."
			git clone --depth 1 https://github.com/Linaro/uadk_engine.git
		fi
		cd ${WORKSPACE}/uadk_engine
		;;
	*)
		echo "Invalid repo name: $1"
		return 1
		;;
	esac
	return 0
}

# Delete uadk and ssluadk repo while build is finished.
clean_repo() {
	rm -fr ${WORKSPACE}/uadk
	rm -fr ${WORKSPACE}/uadk_engine
}

build_uadk() {
	echo "Prepare to build UADK repo."
	exit_code=0
	check_existed_repo uadk || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		return $exit_code
	fi

	cd ${WORKSPACE}/uadk
	autoreconf -vfi

	# static build for v2
	exit_code=0
	./configure \
	  --host aarch64-linux-gnu \
	  --target aarch64-linux-gnu \
	  --prefix=${WORKSPACE}/uadk-static-v2/usr/local \
	  --includedir=${WORKSPACE}/uadk-static-v2/${INC_DIR}/uadk \
	  --enable-static --disable-shared --with-static_drv
	make -j$(nproc) || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to build UADK statically."
		return $exit_code
	fi
	make install
	exit_code=0
	sudo \
	  LD_LIBRARY_PATH=${WORKSPACE}/uadk-static-v2/usr/local/lib/ \
	  PATH=${WORKSPACE}/uadk-static-v2/usr/local/bin:${PATH}  \
	  C_INCLUDE_PATH=${WORKSPACE}/uadk-static-v2/usr/local/include/ \
	  ${WORKSPACE}/uadk/test/sanity_test.sh || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to run UADK tests with static build."
	fi

	sh cleanup.sh
	autoreconf -vfi

	# shared build for v2
	exit_code=0
	./configure \
	  --host aarch64-linux-gnu \
	  --target aarch64-linux-gnu \
	  --prefix=${WORKSPACE}/uadk-shared-v2/usr/local \
	  --includedir=${WORKSPACE}/uadk-shared-v2/${INC_DIR}/uadk \
	  --disable-static --enable-shared
	make -j$(nproc) || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to build UADK statically."
		return $exit_code
	fi
	make install
	exit_code=0
	sudo \
	  LD_LIBRARY_PATH=${WORKSPACE}/uadk-shared-v2/usr/local/lib/ \
	  PATH=${WORKSPACE}/uadk-shared-v2/usr/local/bin:${PATH}  \
	  C_INCLUDE_PATH=${WORKSPACE}/uadk-shared-v2/usr/local/include/ \
	  ${WORKSPACE}/uadk/test/sanity_test.sh || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to run UADK tests with dynamic build."
	fi

	return 0
}

build_openssl() {
	exit_code=0
	check_prebuilt_package openssl || exit_code=$?
	if [ "$exit_code" = 0 ]; then
		return 0
	fi

	./config --prefix=${LIB_ROOT}/usr/local
	exit_code=0
	make -j$(nproc) || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to build OpenSSL ($exit_code)"
		return $exit_code
	fi
	make install || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to install OpenSSL ($exit_code)"
		return $exit_code
	fi

	# get the first 8 bytes of commit number
	COMMIT=`git log HEAD^..HEAD | grep commit`
	COMMIT=${COMMIT#*commit }
	COMMIT=`echo ${COMMIT} | cut -b 1-8`
	cd ${WORKSPACE}
	# clean old packages
	rm -f openssl-*.tar.xz || exit_code=$?
	echo "rm ssl $exit_code"
	# create new package
	tar -cJf openssl-${COMMIT}.tar.xz openssl || exit_code=$?
	echo "tar ssl $exit_code"
	return 0
}

build_uadk_openssl() {
	echo "Prepare to build OPENSSL-UADK repo."
	exit_code=0
	check_existed_repo ssluadk || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		return $exit_code
	fi

	cd ${WORKSPACE}/uadk_engine
	autoreconf -i -f -v
	./configure --prefix=${LIB_ROOT}/usr/local \
		--libdir=${LIB_ROOT}/${LIB_DIR}/engines-1.1

	exit_code=0
	LD_LIBRARY_PATH=${LIB_ROOT}/${LIB_DIR}:${LIB_ROOT}/${LIB_DIR}/engines-1.1 \
	  PATH=${LIB_ROOT}/${BIN_DIR}:${PATH} \
	  C_INCLUDE_PATH=${LIB_ROOT}/${INC_DIR} \
	  make || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to build uadk_engine ($exit_code)"
		return $exit_code
	fi
	exit_code=0
	make install || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to install uadk_engine ($exit_code)"
		return $exit_code
	fi
	exit_code=0
	sudo \
	  LD_LIBRARY_PATH=${LIB_ROOT}/${LIB_DIR}:${LIB_ROOT}/${LIB_DIR}/engines-1.1 \
	  PATH=${LIB_ROOT}/${BIN_DIR}/:${PATH} \
	  C_INCLUDE_PATH=${LIB_ROOT}/${INC_DIR}/ \
	  openssl engine -t uadk_engine || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to verify uadk_engine engine ($exit_code)"
		return $exit_code
	fi
	exit_code=0
	sudo \
	  LD_LIBRARY_PATH=${LIB_ROOT}/${LIB_DIR}:${LIB_ROOT}/${LIB_DIR}/engines-1.1 \
	  PATH=${LIB_ROOT}/${BIN_DIR}/:${PATH} \
	  C_INCLUDE_PATH=${LIB_ROOT}/${INC_DIR}/ \
	  test/sanity_test.sh || exit_code=$?
	if [ "$exit_code" != 0 ]; then
		echo "Fail to run sanity test ($exit_code)"
		return $exit_code
	fi

	return $exit_code
}

# Initialize trap to call ctrlc_handler function when signal 2 (SIGINT) is
# received.
trap "ctrlc_handler" 2

if [ -z "${WORKSPACE}" ]; then
  # Local build
  export WORKSPACE=${PWD}
fi
LIB_ROOT=${WORKSPACE}/uadk-shared-v2

# Prevent multiple building scripts to access hardware
lock

# Load UACCE kernel modules, since there're different configurations of
# UACCE kernel modules for UADK v1 and UADK v2.
sh /home/buildslave/uadk-kernel/launch_uacce.sh || exit_code=$?
if [ "$exit_code" != 0 ]; then
	unlock
	echo "Error while load UACCE modules."
	exit 1
fi

echo "#${BUILD_NUMBER}-${ghprbActualCommit:0:8}" > ${WORKSPACE}/version.txt

# Build dependencies already pre-installed on the node
#sudo apt update -q=2
#sudo apt install -q=2 --yes --no-install-recommends zlib1g-dev libnuma-dev

exit_code=0

# Build UADK
build_uadk || exit_code=$?
if [ "$exit_code" != 0 ]; then
	clean_repo
	unlock
	exit 1
fi
# Build OPENSSL
build_openssl || exit_code=$?
if [ "$exit_code" != 0 ]; then
	clean_repo
	unlock
	exit 1
fi
# Build UADK-OPENSSL
build_uadk_openssl || exit_code=$?
if [ "$exit_code" != 0 ]; then
	clean_repo
	unlock
	exit 1
fi

clean_repo
unlock
echo "Run build script successfully!"
