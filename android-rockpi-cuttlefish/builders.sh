
# Install needed packages
sudo apt-get update
sudo apt-get install -y gcc-aarch64-linux-gnu gcc-arm-none-eabi debootstrap qemu-user-static curl lz4 cpio qemu-system-arm rsync procps gdisk xz-utils

# Download helper scripts (repo)
mkdir -p ${HOME}/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ${HOME}/bin/repo
chmod a+x ${HOME}/bin/*
export PATH=${HOME}/bin:${PATH}:/sbin:/usr/sbin

# Build Android cuttlefish rockpi
export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8

# change to the build directory to repo sync and build
cd build
mkdir -p u-boot-mainline
cd u-boot-mainline
repo init -u ${UBOOT_MANIFEST_URL} -b ${UBOOT_MANIFEST_BRANCH}
repo sync
cd ..
mkdir -p kernel
cd kernel
repo init -u ${KERNEL_MANIFEST_URL} -b ${KERNEL_MANIFEST_BRANCH}
repo sync
cd ..
mkdir -p aosp
repo init -u ${ANDROID_MANIFEST_URL} -b ${ANDROID_MANIFEST_BRANCH}
repo sync
cd ..

rm -rf output/
mkdir -p output/host/image
mkdir -p pub

cd aosp
repo manifest -r -o ../pub/pinned-manifest.xml

source build/envsetup.sh
lunch ${LUNCH_TARGET}

wget -q https://git.linaro.org/ci/job/configs.git/blob_plain/HEAD:/android-lcr/generic/build-info/public-template.txt -O ../pub/BUILD-INFO.txt

# Build image
./device/google/cuttlefish/tools/create_base_image_arm.sh ../u-boot-mainline ../kernel ../output/host/image/test.img

xz -9e ../output/host/image/test.img

cp ../output/host/image/test.img.xz ../pub/

# Delete sources after build to save space
rm -rf art/ dalvik/ kernel/ bionic/ developers/ libcore/ sdk/ bootable/ development/ libnativehelper/ system/ build/ device/ test/ build-info/ docs/ packages/ toolchain/ .ccache/ external/ pdk/ tools/ compatibility/ frameworks/ platform_testing/ vendor/ cts/ hardware/ prebuilts/ linaro* ../output
rm -fr .repo

cd ..

# need to convert '_' to '-'
# otherwise, aosp_arm64-userdebug will be translated to '~aosp/arm64-userdebug'
# when upload to snapshot.linaro.org via linaro-cp.py
# like reported here:
# https://ci.linaro.org/job/android-cts/20/console
lunch_target_str=$(echo ${LUNCH_TARGET}|tr '_' '-')
# Publish parameters
cat << EOF > ${WORKSPACE}/publish_parameters
PUB_SRC=${PWD}/pub
PUB_DEST=/android/${JOB_NAME}/${lunch_target_str}/${BUILD_NUMBER}/${ANDROID_MANIFEST_BRANCH}
PUB_EXTRA_INC=^[^/]+xz
EOF
