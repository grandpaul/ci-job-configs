        - shell:
            !include-raw:
                - android-rockpi-cuttlefish/builders.sh
        - inject:
            properties-file: publish_parameters
        - linaro-publish-token
        - shell:
            !include-raw:
                - android/linaro-publisher.sh
