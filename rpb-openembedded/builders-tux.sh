#!/bin/bash

set -ex

# install jflog client tool, v1, used for publishing artifacts
(mkdir -p $HOME/bin && cd $HOME/bin && curl -fL https://getcli.jfrog.io | sh)

# Get tuxsuite
virtualenv --python=$(which python3) .venv
source .venv/bin/activate
pip install tuxsuite
tuxsuite --version


[ "${DISTRO}" = "rpb" ] && IMAGES+=" ${IMAGES_RPB}"
[ "${DISTRO}" = "rpb-wayland" ] && IMAGES+=" ${IMAGES_RPB_WAYLAND}"

# These machines only build the basic rpb-console-image
case "${MACHINE}" in
  am57xx-evm|intel-core2-32|intel-corei7-64)
     IMAGES="rpb-console-image"
     ;;
esac

# Default value for BB_OVERRIDE is set to new syntax
if [ -z "${BB_OVERRIDE}" ]; then
    BB_OVERRIDE=':'
fi

# Create Tux Job file
cat << EOF > tux.json
{
  "sources": {
    "repo": {
        "url": "${MANIFEST_URL}",
        "branch": "${MANIFEST_BRANCH}",
        "manifest": "default.xml"
      }
  },
  "container": "ubuntu-20.04",
  "envsetup": "setup-environment",
  "distro": "${DISTRO}",
  "machine": "${MACHINE}",
  "target": "${IMAGES}",
  "artifacts": [],
    "environment": {
  },
  "local_conf": [
    "INHERIT += 'buildstats buildstats-summary'",
    "INHERIT${BB_OVERRIDE}remove = 'rm_work'",
    "IMAGE_NAME${BB_OVERRIDE}append = '-${BUILD_NUMBER}'",
    "KERNEL_IMAGE_NAME${BB_OVERRIDE}append = '-${BUILD_NUMBER}'",
    "MODULE_TARBALL_NAME${BB_OVERRIDE}append = '-${BUILD_NUMBER}'",
    "DT_IMAGE_BASE_NAME${BB_OVERRIDE}append = '-${BUILD_NUMBER}'",
    "BOOT_IMAGE_BASE_NAME${BB_OVERRIDE}append = '-${BUILD_NUMBER}'"
  ]
}
EOF

# Process top level job extra local.conf
# This jq script will take the main JSON file and merge local_conf data
if [ -f "${WORKSPACE}/local.conf.json" ]; then
    cp tux.json tux.orig.json
    jq 'reduce inputs.local_conf as $s (.; .local_conf += $s)' tux.orig.json "${WORKSPACE}/local.conf.json" > tux.json
fi

# Build, do not report tuxsuite return code, since we want to process json out
tuxsuite bake submit --json-out status.json tux.json || true

# cleanup virtualenv
deactivate

if [ ! -f status.json ]; then
    echo "tux suite failed, infrastructure error, status.json does not exist"
    exit 1
fi

url=$(cat status.json | jq -r ".download_url")
state=$(cat status.json | jq -r ".state")
result=$(cat status.json | jq -r ".result")

if [ "$state" != "finished" ]; then
    echo "tuxsuite failed, with an unexpected reason"
    exit 1
fi

wget $url/build.log
cat build.log

echo "TUXBUILD_URL=$url" > parameters

if [ "$result" != "pass" ]; then
    echo "tuxsuite build failed"
    exit 2
fi

# Prepare files to publish, if needed
if [ -z "${PUBLISH_SERVER}" ]; then
    exit 0
fi

DEPLOY_DIR_IMAGE=${PWD}/deploy_dir_images
echo "DEPLOY_DIR_IMAGE=${DEPLOY_DIR_IMAGE}" >> parameters

# files to download
wget $url/?export=json -O root.json
wget $url/images/${MACHINE}/?export=json -O images.json

mkdir ${DEPLOY_DIR_IMAGE} && pushd ${DEPLOY_DIR_IMAGE}

for j in ${WORKSPACE}/root.json ${WORKSPACE}/images.json; do
    for f in $(cat $j | jq -r .files[].Url); do
	wget $f
    done
done
popd

# Create MD5SUMS file
find ${DEPLOY_DIR_IMAGE} -type f | xargs md5sum > MD5SUMS.txt
sed -i "s|${DEPLOY_DIR_IMAGE}/||" MD5SUMS.txt
mv MD5SUMS.txt ${DEPLOY_DIR_IMAGE}
