#!/bin/bash -f

set -e

# Processing script arguments
verbose=true
config_dir=configs
job_subset=''
result_dir="."
while [[ $# -gt 0 ]]; do
  case $1 in
     --verbose) 
       verbose=$2;
       shift ;;
     --job_subset)
       job_subset=$2
       shift ;;
     --config_dir)
       config_dir=$2
       shift ;;
     --result_dir)
       result_dir=$2
       shift ;;
    -*)
      echo "Unknown option $1"
      exit 1
  esac
  shift
done

jenkinsurl=https://ci.linaro.org
exit_status=0

ci_jobs=()
configs_jobs=()

mkdir -p "$result_dir"

if $verbose; then
  DEBUGFILE=$result_dir/check_ci_sync_debug.txt
  rm -f "$DEBUGFILE"
else
  DEBUGFILE=/dev/null
fi


## GET THE JOBS LIST LOOKING AT JENKINS SERVER
get_ci_jobs()
{
   echo "==============  GET_CI_JOBS  ==============="		| tee -a "$DEBUGFILE"

   # ssh ci.linaro.org list-jobs
   CMD="ssh -p2222 -l $USER@linaro.org ci.linaro.org"
   echo "$CMD list-jobs | grep $job_subset"

   list_jobs_file=$(mktemp)
   $CMD list-jobs > $list_jobs_file
   while read -r jb; do
      if [[ $jb == *$job_subset* ]]; then 
         echo " * $jb"    					>> "$DEBUGFILE"
         ci_jobs+=( "$jb" )
      else
         echo "   $jb"    					>> "$DEBUGFILE"
      fi
   done < $list_jobs_file

   printf "  (found $(cat $list_jobs_file|wc -l) jobs, select ${#ci_jobs[*]} jobs)\n"
   echo ""
   rm -f $list_jobs_file
}

## GET THE JOBS LIST LOOKING AT GIT CONFIG
get_configs_jobs()
{
   configs_jobs_failing=()
   outdir="configs_jobs_out"
   [ -d $outdir ] && rm -rf $outdir
   mkdir $outdir

   echo "============  GET_CONFIGS_JOBS  ============="		| tee -a "$DEBUGFILE"
   # Yaml files
   yaml_files=$(find $config_dir -name "*$job_subset*.yaml")
   printf "  (found $(echo $yaml_files | wc -w) yaml files)\n"

   # jenkins-job test <every file>
   jjb_output_file=$(mktemp)
   for fn in $yaml_files; do
     if ! jenkins-jobs test -o $outdir $fn >& $jjb_output_file; then
	echo ""
	echo   "# WARNING: jenkins-job command fails"
	echo " + jenkins-jobs test -o $outdir $fn  # Fails"	| tee -a "$DEBUGFILE"
	cat "$jjb_output_file"
	configs_jobs_failing+=( "$fn" )
     else
	echo "+ jenkins-jobs test -o $outdir $fn"		>> "$DEBUGFILE"
     fi
     rm "$jjb_output_file"
   done

   # Select the jobs, not the views
   for fn in $(ls -1 $outdir); do
      if grep -q "hudson.model.ListView" $outdir/$fn; then
         configs_views+=( "$fn" )
      else
         configs_jobs+=( "$fn" )
      fi
   done
   echo ""
   echo "  (found ${#configs_jobs[*]} jobs, ${#configs_views[*]} views, ${#configs_jobs_failing[*]} jjb failing)"

   # Print failing jenkins-job if any
   if [ ${#configs_jobs_failing[@]} -gt 0 ]; then
     echo "# jenkins-jobs failing for file :"			| tee -a "$DEBUGFILE"
     for fn in ${configs_jobs_failing[*]}; do
       echo "  jenkins-jobs test -o $outdir $fn"		| tee -a "$DEBUGFILE"
     done
     echo ""
   fi
   echo ""
}

yaml_exists_job_noexists=()
yaml_noexists_job_exists=()
yaml_exists_job_exists=()

## COMPARE AND PRINT THE SUMMARY
summary()
{
   rm -f "$DEBUGFILE"

   echo "============  SUMMARY  ============="
   sorted_jobs=( $(echo "${configs_jobs[@]}" "${ci_jobs[@]}"|xargs -n1|sort -u|xargs) )

   for jb in "${sorted_jobs[@]}"; do
      if [[ ${configs_jobs[*]} =~ (^|[[:space:]])"$jb"($|[[:space:]]) ]] &&
         [[ ${ci_jobs[*]} =~ (^|[[:space:]])"$jb"($|[[:space:]]) ]]; then
	 yaml_exists_job_exists+=( "$jb" )
	 printf " %-100s | %15s | %15s |\n" "$jb" "yaml exists" "job exists"	>> "$DEBUGFILE"
      fi
      if [[ ! ${configs_jobs[*]} =~ (^|[[:space:]])"$jb"($|[[:space:]]) ]] &&
         [[ ${ci_jobs[*]} =~ (^|[[:space:]])"$jb"($|[[:space:]]) ]]; then
	 yaml_noexists_job_exists+=( "$jb" )
	 printf " %-100s | %15s | %15s |\n" "$jb" "yaml NO exists" "job exists"	>> "$DEBUGFILE"
      fi
      if [[ ${configs_jobs[*]} =~ (^|[[:space:]])"$jb"($|[[:space:]]) ]] &&
         [[ ! ${ci_jobs[*]} =~ (^|[[:space:]])"$jb"($|[[:space:]]) ]]; then
	 yaml_exists_job_noexists+=( "$jb" )
	 printf " %-100s | %15s | %15s |\n" "$jb" "yaml exists" "job NO exists"	>> "$DEBUGFILE"
      fi
   done
   echo "JENKINS JOBS exists and YAML FILE exist             : ${#yaml_exists_job_exists[@]} jobs"	| tee -a "$DEBUGFILE"
   echo "JENKINS JOBS doesnot exists but the YAML FILE exist : ${#yaml_noexists_job_exists[@]} jobs"	| tee -a "$DEBUGFILE"
   echo "JENKINS JOBS exists but the YAML FILE doesnot exist : ${#yaml_exists_job_noexists[@]} jobs"	| tee -a "$DEBUGFILE"

   if [ ${#yaml_noexists_job_exists[@]} != 0 ] || [ ${#yaml_exists_job_noexists[@]} != 0 ]; then
      exit_status=1
   fi

   echo "  (See details in $DEBUGFILE)"
   echo ""
}

## PRINT SUGGESTIONS TO RESOLVE THESE GAP
print_suggestions()
{
   CMD="ssh -p2222 -l \$USER@linaro.org ci.linaro.org"

   echo "============  SUGGESSTIONS  ============="

   if [ ${#yaml_noexists_job_exists[@]} != 0 ]; then
      echo "# Some JENKINS JOBS exists but the YAML FILE doesnot exist (${#yaml_noexists_job_exists[@]} jobs)"
      echo "# --------------------------------------------------------"
      echo "#   These are orphan jobs that are not linked to any yaml files."
      echo "#   You could delete them by using commands below.              "
      echo "#   Please make sure that's what you want before deleting.      "
      if [ ${#configs_jobs_failing[@]} != 0 ]; then
         echo "#    /!\ WARNING : some of the following orphan jobs may be due to the failing jenkins-job commands above."
      fi

      echo ""
      for jb in "${yaml_noexists_job_exists[@]}"; do
	 echo "    # $jenkinsurl/job/$jb"
	 echo "    $CMD delete-job $jb"
      done
      echo ""
   fi

   if [ ${#yaml_exists_job_noexists[@]} != 0 ]; then
      echo "# Some YAML FILES generates a xml, but the JENKINS JOB doesnot exist (${#yaml_noexists_job_exists[@]} jobs)"
      echo "# --------------------------------------------------------"
      echo "#   These jobs might probably exists, but are not readable by $USER."

      echo ""
      for jb in "${yaml_exists_job_noexists[@]}"; do
	 echo "    # $jenkinsurl/job/$jb"
      done
      echo ""
   fi
}



get_ci_jobs

get_configs_jobs

summary

print_suggestions

echo "exit $exit_status"
exit $exit_status
