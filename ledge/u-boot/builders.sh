#!/bin/bash

echo "LEDGE build for machine ${MACHINE} distro ${DISTRO}"
ORIG_MACHINE="${MACHINE}"

set -e

# workaround EDK2 is confused by the long path used during the build
# and truncate files name expected by VfrCompile
sudo mkdir -p /srv/oe
sudo chown buildslave:buildslave /srv/oe
cd /srv/oe

trap cleanup_exit INT TERM EXIT

cleanup_exit()
{
    echo "Running cleanup_exit..."
}

set -ex

# Store the home repository
if [ -z "${WORKSPACE}" ]; then
  # Local build
  export WORKSPACE=${PWD}
fi

# Install toolchain
if ! sudo DEBIAN_FRONTEND=noninteractive apt-get -q=2 update; then
  echo "INFO: apt update error - try again in a moment"
  sleep 15
  sudo DEBIAN_FRONTEND=noninteractive apt-get -q=2 update || true
fi
pkg_list="gcc-aarch64-linux-gnu"
if ! sudo DEBIAN_FRONTEND=noninteractive apt-get -q=2 install -y ${pkg_list}; then
  echo "INFO: apt install error - try again in a moment"
  sleep 15
  sudo DEBIAN_FRONTEND=noninteractive apt-get -q=2 install -y ${pkg_list}
fi


#Build
git clone https://github.com/u-boot/u-boot.git
cd u-boot
make ARCH=arm CROSS_COMPILE=aarch64-linux-gnu- qemu_arm64_defconfig
make ARCH=arm CROSS_COMPILE=aarch64-linux-gnu-

export UPLOAD_DIR="`pwd`/../uboot-build"
mkdir -p ${UPLOAD_DIR}
cp u-boot.bin ${UPLOAD_DIR}/u-boot.aarch64.bin

BASE_URL=http://snapshots.linaro.org

#Trigger LAVA job
cat > ${UPLOAD_DIR}/build_config.json <<EOF
{
  "branch" : "master",
  "build_location" : "${BASE_URL}/${PUB_DEST}"
}
EOF

cat << EOF > ${WORKSPACE}/post_build_lava_parameters
DEPLOY_DIR_IMAGE=${UPLOAD_DIR}
UBOOT="${BASE_URL}/${PUB_DEST}/u-boot.aarch64.bin"
EOF

cat ${WORKSPACE}/post_build_lava_parameters
