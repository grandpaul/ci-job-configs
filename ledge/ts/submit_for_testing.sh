#!/bin/bash

case $MACHINE in
	stm32mp157c-dk2)
		echo "running test for ${MACHINE}"
	;;
	*)
		echo "skiping test for ${MACHINE}"
		exit 0
	;;
esac

sudo locale-gen en_US.UTF-8 && sudo update-locale LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

[ -z "${KSELFTEST_PATH}" ] && export KSELFTEST_PATH="/opt/kselftests/mainline/"
[ -z "${LAVA_JOB_PRIORITY}" ] && export LAVA_JOB_PRIORITY="25"
[ -z "${SANITY_LAVA_JOB_PRIORITY}" ] && export SANITY_LAVA_JOB_PRIORITY="30"
[ -z "${SKIP_LAVA}" ] || unset DEVICE_TYPE
[ -z "${QA_SERVER_TEAM}" ] && export QA_SERVER_TEAM=rpb
[ -z "${TOOLCHAIN}" ] && export TOOLCHAIN="unknown"
[ -z "${TDEFINITIONS_REVISION}" ] && export TDEFINITIONS_REVISION="kselftest-5.1"
[ -z "${MANIFEST_COMMIT}" ] && export MANIFEST_COMMIT="HEAD"
[ -z "${MANIFEST_BRANCH}" ] && export MANIFEST_BRANCH="unknown"


rm -rf configs
git clone --depth 1 http://git.linaro.org/ci/job/configs.git


python3 configs/openembedded-lkft/submit_for_testing.py \
         --device-type ${MACHINE} \
         --build-number ${BUILD_NUMBER} \
         --lava-server ${LAVA_SERVER} \
         --qa-server ${QA_SERVER} \
         --qa-server-team ${QA_SERVER_TEAM} \
         --qa-server-project ${QA_SERVER_PROJECT} \
         --git-commit ${MANIFEST_COMMIT} \
         --template-path configs/ledge/ts/lava-job-definitions \
         --template-names template-boot.yaml
