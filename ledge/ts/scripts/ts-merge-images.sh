#!/bin/bash
firmware_image=$1
os_image=$2

set -e

export LANG=C

[ "$#" -ne 2 ] && echo "$0 <firmware .wic> <rootfs .wic>" && exit 1

# src esp size
src_start=`fdisk -l ${os_image} | grep wic1 | awk '{print $2}'` # in sectors
src_end=`fdisk -l ${os_image} | grep wic1 | awk '{print $3}'` # in sectors
src_size=$((src_end - src_start + 1)) # in sectors

# rootfs size
rootfs_start=`fdisk -l ${os_image} | grep wic2 | awk '{print $2}'` # in sectors
rootfs_end=`fdisk -l ${os_image} | grep wic2 | awk '{print $3}'` # in sectors
rootfs_size=$((rootfs_end - rootfs_start + 1)) # in sectors

extra_size=$(((src_size + rootfs_size) * 512 + 10*1024*1024)) # 10MB extra at the end

truncate $firmware_image --size +${extra_size}
#fix image after resize with sgdisk + fdisk
sgdisk -e $firmware_image
fdisk $firmware_image <<EOF
w
EOF

# Create new ESP and rootfs partitions
fdisk $firmware_image <<EOF
n


+$((src_size - 1))
t

1
n


+$((rootfs_size - 1))
t

20
w
EOF
fdisk -l $firmware_image

esp_dest_start=`fdisk -l ${firmware_image} | tail -n 2 | head -n 1 | awk '{print $2}'`

dd conv=notrunc \
	bs=512 \
	count=${src_size} \
	skip=${src_start} \
	seek=${esp_dest_start} \
	if=${os_image} \
	of=${firmware_image} \
	status=progress


rootfs_dest_start=`fdisk -l ${firmware_image} | tail -n 1 | awk '{print $2}'`
dd conv=notrunc \
	bs=512 \
	count=${rootfs_size} \
	skip=${rootfs_start} \
	seek=${rootfs_dest_start} \
	if=${os_image} \
	of=${firmware_image} \
	status=progress
