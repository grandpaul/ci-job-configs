#!/bin/bash
firmware_image=$1
os_image=$2

set -e

export LANG=C

[ "$#" -ne 2 ] && echo "$0 <firmware .wic> <rootfs .wic>" && exit 1

# src esp size
src_start=`fdisk -l ${os_image} | grep wic1 | awk '{print $2}'` # in sectors
src_end=`fdisk -l ${os_image} | grep wic1 | awk '{print $3}'` # in sectors
src_size=$((src_end - src_start + 1)) # in sectors

truncate $firmware_image --size +$((src_size * 512))
#fix image after resize with sgdisk + fdisk
sgdisk -e $firmware_image
fdisk $firmware_image <<EOF
w
EOF
fdisk -l ${firmware_image}

# Create new ESP partition with SRC size and EFI type
fdisk $firmware_image <<EOF
n


+$((src_size - 1))
t

1
w
EOF
fdisk -l $firmware_image

esp_dest_start=`fdisk -l ${firmware_image} | tail -n 1 | awk '{print $2}'`

dd conv=notrunc \
	bs=512 \
	count=${src_size} \
	skip=${src_start} \
	seek=${esp_dest_start} \
	if=${os_image} \
	of=${firmware_image} \
	status=progress
