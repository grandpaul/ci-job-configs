#!/bin/bash

echo "Trusted Substrate Build for: ${MACHINE}"

set -e

sudo mkdir -p /srv/oe
sudo chown buildslave:buildslave /srv/oe
cd /srv/oe

trap cleanup_exit INT TERM EXIT

cleanup_exit()
{
    echo "Running cleanup_exit..."
}

set -ex

# Store the home repository
if [ -z "${WORKSPACE}" ]; then
  # Local build
  export WORKSPACE=${PWD}
fi

git clone ${GIT_URL} -b ${GIT_BRANCH}
cd meta-ts

# link to shared downloads on persistent disk
# our builds config is expecting downloads and sstate-cache, here.
mkdir -p build/downloads ${HOME}/srv/oe/sstate-cache-${GIT_BRANCH}
mkdir -p build/sstate-cache
ln -s ${HOME}/srv/oe/downloads build/downloads
ln -s ${HOME}/srv/oe/sstate-cache-${GIT_BRANCH} sstate-cache
rm -rf build/conf build/tmp

GIT_COMMIT=`git rev-parse HEAD`

kas build ci/${MACHINE}.yml

DEPLOY_DIR_IMAGE="`pwd`/build/tmp/deploy/images/${MACHINE}"
UPLOAD_DIR_IMAGE="`pwd`/build/tmp/deploy/images/${MACHINE}-upload"
mkdir -p ${UPLOAD_DIR_IMAGE}

case ${MACHINE} in
  qemuarm64-secureboot)
	DEPLOY_DIR_IMAGE="`pwd`/build/tmp/deploy/images/ts${MACHINE}"
	find ${DEPLOY_DIR_IMAGE} -name flash.bin -type l -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	;;
  zynqmp-starter)
	find ${DEPLOY_DIR_IMAGE} -name ImageA.bin -type f -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	find ${DEPLOY_DIR_IMAGE} -name ImageB.bin -type f -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	;;
  synquacer)
	find ${DEPLOY_DIR_IMAGE} -name scp_romramfw_release.bin -type f -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	find ${DEPLOY_DIR_IMAGE} -name u-boot.bin 		-type l -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	find ${DEPLOY_DIR_IMAGE} -name fip_all_arm_tf_optee.bin -type f -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	find ${DEPLOY_DIR_IMAGE} -name tee-pager_v2.bin 	-type f -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	;;
  stm32mp157c-dk2)
	find ${DEPLOY_DIR_IMAGE} -name ts-firmware-${MACHINE}.wic.gz -type l -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	pushd ${UPLOAD_DIR_IMAGE}
	  wget http://releases.linaro.org/components/ledge/rp-0.3/ledge-multi-armv7/ledge-qemuarm/ledge-iot-lava-ledge-qemuarm-20211202164307.rootfs.wic.gz
	  cp ts-firmware-${MACHINE}.wic.gz sdcard.bin.gz
	  gunzip ledge-iot-lava-ledge-qemuarm-*.rootfs.wic.gz  sdcard.bin.gz
	  wget http://git-us.linaro.org/ci/job/configs.git/plain/ledge/ts/scripts/ts-merge-images.sh
	  rootfsimg=`ls ledge-iot-lava-ledge-qemuarm-*.rootfs.wic`
	  sudo sh -x ./ts-merge-images.sh sdcard.bin ${rootfsimg}
	  gzip sdcard.bin
	  rm -rf ${rootfsimg} ts-merge-images.sh
	popd
	URL_STM_SDIMAGE="http://snapshots.linaro.org/${PUB_DEST}/sdcard.bin.gz"
        ;;
  *)
	find ${DEPLOY_DIR_IMAGE} -name ts-firmware-${MACHINE}.wic.gz -type l -exec cp -rvfL --remove-destination {} ${UPLOAD_DIR_IMAGE} \;
	;;
esac

# Note: the main job script allows to override the default value for
#       BASE_URL and PUB_DEST, typically used for OE RPB builds
cat << EOF > ${WORKSPACE}/post_build_lava_parameters
DEPLOY_DIR_IMAGE=${UPLOAD_DIR_IMAGE}
URL_STM_FLASHER="http://git-us.linaro.org/ci/job/configs.git/plain/ledge/ts/scripts/stm32.tar.gz"
URL_STM_TSV="http://git-us.linaro.org/ci/job/configs.git/plain/ledge/ts/scripts/stm32mp157c-sdcard.tsv"
URL_STM_SDIMAGE=${URL_STM_SDIMAGE}
BUILD_NUMBER=${BUILD_NUMBER}
MACHINE=${MACHINE}
GIT_URL=${GIT_URL}
GIT_BRANCH=${GIT_BRANCH}
GIT_COMMIT=${GIT_COMMIT}
PUB_DEST=${PUB_DEST}
EOF
