#!/bin/bash
set -ex

# get rescue image & unzip it
ZIP_IMG_NAME=$(basename ${RESCUE_IMG_URL})
(wget ${RESCUE_IMG_URL} -O ${ZIP_IMG_NAME} ; unzip -o '*.zip')

# list all loop devices
sudo losetup --all

# attach
RESCUE_DIR=$(basename ${RESCUE_IMG_URL} .zip)
RESCUE_IMG=$(ls -1 ${RESCUE_DIR}/*.img)
DPATH=$(sudo losetup --show -f ${RESCUE_IMG} -o 17408 --sizelimit 524288)

# detach
sudo losetup -d ${DPATH}

# clean up
rm -rf ${RESCUE_DIR} ${ZIP_IMG_NAME}
