- string:
    name: IMAGES_RPB
    default: 'rpb-console-image rpb-console-image-test rpb-desktop-image rpb-desktop-image-test'
- string:
    name: IMAGES_RPB_WAYLAND
    default: 'rpb-console-image rpb-console-image-test rpb-weston-image rpb-weston-image-test'
- string:
    name: MANIFEST_URL
    default: 'https://github.com/96boards/oe-rpb-manifest.git'
- string:
    name: MANIFEST_BRANCH
    default: 'dunfell'
- string:
    name: BASE_URL
    default: 'http://snapshots.linaro.org/'
